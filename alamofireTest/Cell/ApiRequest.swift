//
//  ApiRequest.swift
//  alamofireTest
//
//  Created by Beetech on 7/6/20.
//  Copyright © 2020 Beetech. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Alamofire
class ApiRequest {

    private let baseUrl: String = "https://api.github.com/users/shuuun/repos"

    func getResponse() -> Observable<DataResponse<JSON>> {
        return Observable.create { observer in
            Alamofire.request(self.baseUrl)
                .responseSwiftyJSON { response in
                    print("JSONあげるわ")
                    observer.onNext(response)
                    observer.onCompleted()
            }
            return Disposables.create()
        }
    }
    func getApi() -> Observable<AnyObject?> {
        return Observable.create { observer in
            let request = AF.request( "http://someapiurl.com", parameters: nil)
                .response(completionHandler:  { request, response, data, error in
                    if ((error) != nil) {
                        observer.on(.Error(error!))
                    } else {
                        observer.on(.Next(data))
                        observer.on(.Completed)
                    }
                });
            return AnonymousDisposable {
                request.cancel()
            }
        }
    }
}
