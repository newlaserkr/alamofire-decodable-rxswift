//
//  DrinkCell.swift
//  alamofireTest
//
//  Created by Beetech on 7/7/20.
//  Copyright © 2020 Beetech. All rights reserved.
//

import UIKit
import SDWebImage

class DrinkCell: UITableViewCell {
    
    @IBOutlet weak var imgCocktail: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func parseData(imageLink: String?, titleString: String?, descriptionString:String?){
         imgCocktail.sd_setImage(with: URL(string: imageLink ?? ""))
         titleLabel.text = titleString ?? ""
         descriptionLabel.text = descriptionString ?? ""
     }
}
