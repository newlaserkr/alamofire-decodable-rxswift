//
//  CocktailViewModel.swift
//  alamofireTest
//
//  Created by Beetech on 7/6/20.
//  Copyright © 2020 Beetech. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa
import SwiftyJSON

class CocktailViewModel: NSObject{
    
    private let baseURL = "https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a"
    
    override init() {
        super.init()
    }
    
    func getResponse() -> Observable<[Drinks]> {
        return Observable.create { observer in
            AF.request(self.baseURL, method: .get)
                .response { response in
                    switch response.result {
                    case .success(let value):
                        do {
                            let product = try JSONDecoder().decode(Cocktail.self, from: value!)
                            observer.onNext(product.drinks ?? [])
                            observer.onCompleted()
                        }catch let err{
                            print("Error with decode data: ",err)
                        }
                        
                    case .failure(let error):
                        observer.onError(error)
                    }
            }
            return Disposables.create()
        }
    }
    
}
