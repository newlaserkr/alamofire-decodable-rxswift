//
//  ViewController.swift
//  alamofireTest
//
//  Created by Beetech on 7/6/20.
//  Copyright © 2020 Beetech. All rights reserved.
//

import UIKit
import RxSwift
let cell = "DrinkCell"
class ViewController: UIViewController {
    
    @IBOutlet weak var CocktailTableView: UITableView!
    private let bag = DisposeBag()
    private var vm: CocktailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm = CocktailViewModel()
        //register nib
        let nib = UINib(nibName: cell, bundle: nil)
        CocktailTableView.register(nib, forCellReuseIdentifier: cell)
        
        vm?.getResponse().asObservable()
            .bind(to: CocktailTableView.rx.items(cellIdentifier: cell, cellType: DrinkCell.self)){ index,model,cell in
                cell.parseData(imageLink: model.strDrinkThumb, titleString: model.strDrink, descriptionString: model.strCategory)
        }.disposed(by: bag)
        
    }
    
}


