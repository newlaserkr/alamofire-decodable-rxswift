//
//  CocktailModel.swift
//  alamofireTest
//
//  Created by Beetech on 7/6/20.
//  Copyright © 2020 Beetech. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Cocktail: Decodable{
    var drinks : [Drinks]?
}

struct Drinks : Decodable {
    
    var strDrink : String?
    var strCategory : String?
    var strDrinkThumb : String?
}
